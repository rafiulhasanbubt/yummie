//
//  HomeViewController.swift
//  Yummie
//
//  Created by rafiul hasan on 5/11/21.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var popularCollectionView: UICollectionView!
    @IBOutlet weak var chefCollectionView: UICollectionView!
    
    var categories: [DishCategory] = [
        .init(title: "Rafi", image: #imageLiteral(resourceName: "slide1")),
        .init(title: "Rafiul Hasan", image: #imageLiteral(resourceName: "slide1")),
        .init(title: "Rafi", image: #imageLiteral(resourceName: "slide1")),
        .init(title: "Rafi", image: #imageLiteral(resourceName: "slide1")),
        .init(title: "Rafi Rahman", image: #imageLiteral(resourceName: "slide1")),
        .init(title: "Rafi", image: #imageLiteral(resourceName: "slide1")),
        .init(title: "Rafi", image: #imageLiteral(resourceName: "slide1")),
        .init(title: "Rafiul khan", image: #imageLiteral(resourceName: "slide1")),
        .init(title: "Rafi", image: #imageLiteral(resourceName: "slide1")),
        .init(title: "Rafi", image: #imageLiteral(resourceName: "slide1")),
        .init(title: "Rafi", image: #imageLiteral(resourceName: "slide1")),
        .init(title: "Rafi", image: #imageLiteral(resourceName: "slide1"))
    ]
    
    var populars: [Dish] = [
        .init(name: "Rafiul", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", image: #imageLiteral(resourceName: "iTunesArtwork"), calories: 130.722),
        .init(name: "shamim", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", image: #imageLiteral(resourceName: "slide1"), calories: 305.522),
        .init(name: "Sadid", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", image: #imageLiteral(resourceName: "appIcon"), calories: 230.122)
    ]
    
    var specials: [Dish] = [
        .init(name: "Rafiul", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", image: #imageLiteral(resourceName: "iTunesArtwork"), calories: 130.722),
        .init(name: "shamim", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", image: #imageLiteral(resourceName: "slide1"), calories: 305.522),
        .init(name: "Sadid", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", image: #imageLiteral(resourceName: "appIcon"), calories: 230.122)
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NetworkService.shared.myFirstRequest { result in
            switch result {
            case .success(let data):
                for dish in data {
                    print(dish.name )
                }
            case .failure(let error):
                print("The error is \(error.localizedDescription)")
            }
        }
        
        categoryCollectionView.dataSource = self
        categoryCollectionView.delegate = self
        
        popularCollectionView.dataSource = self
        popularCollectionView.delegate = self
        
        chefCollectionView.dataSource = self
        chefCollectionView.delegate = self
        // Do any additional setup after loading the view.
        registerCells()
    }
    
    func registerCells() {
        categoryCollectionView.register(UINib(nibName: CategoryCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: CategoryCollectionViewCell.identifier)
        
        popularCollectionView.register(UINib(nibName: DishPortraitCollectionViewCell.dishIdentifier, bundle: nil), forCellWithReuseIdentifier: DishPortraitCollectionViewCell.dishIdentifier)
        
        chefCollectionView.register(UINib(nibName: ChefSpecialCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: ChefSpecialCollectionViewCell.identifier)
    }

}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
        case categoryCollectionView:
            return categories.count
        case popularCollectionView:
            return populars.count
        case chefCollectionView:
            return specials.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        case categoryCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryCollectionViewCell.identifier, for: indexPath) as! CategoryCollectionViewCell
            cell.setup(category: categories[indexPath.row])
            return cell
        case popularCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DishPortraitCollectionViewCell.dishIdentifier, for: indexPath) as! DishPortraitCollectionViewCell
            cell.dishSetup(dish: populars[indexPath.row])
            return cell
        case chefCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ChefSpecialCollectionViewCell.identifier, for: indexPath) as! ChefSpecialCollectionViewCell
            cell.setup(chef: specials[indexPath.row])
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == categoryCollectionView {
            let controller = ListDishesViewController.instantiate()
            controller.category = categories[indexPath.row]
            navigationController?.pushViewController(controller, animated: true)
        } else {
            let controller = DishDetailViewController.instantiate()
            controller.dish = collectionView == popularCollectionView ? populars[indexPath.row] : specials[indexPath.row]
            navigationController?.pushViewController(controller, animated: true)
        }
    }
}
