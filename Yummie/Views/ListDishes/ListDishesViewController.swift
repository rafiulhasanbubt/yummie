//
//  ListDishesViewController.swift
//  Yummie
//
//  Created by rafiul hasan on 5/11/21.
//

import UIKit

class ListDishesViewController: UIViewController {

    @IBOutlet weak var listDishTableView: UITableView!
    
    var category: DishCategory!
    
    var dishes: [Dish] = [
        .init(name: "Rafiul", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", image: #imageLiteral(resourceName: "iTunesArtwork"), calories: 130.722),
        .init(name: "shamim", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", image: #imageLiteral(resourceName: "slide1"), calories: 305.522),
        .init(name: "Sadid", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", image: #imageLiteral(resourceName: "appIcon"), calories: 230.122)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        listDishTableView.dataSource = self
        listDishTableView.delegate = self
        // Do any additional setup after loading the view.
        title = category.title
        registerCells()
    }
    
    func registerCells() {
        listDishTableView.register(UINib(nibName: DishListTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: DishListTableViewCell.identifier)
    }

}

extension ListDishesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dishes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DishListTableViewCell.identifier, for: indexPath) as! DishListTableViewCell
        cell.setup(dish: dishes[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = DishDetailViewController.instantiate()
        controller.dish = dishes[indexPath.row]
        navigationController?.pushViewController(controller, animated: true)
    }
}
