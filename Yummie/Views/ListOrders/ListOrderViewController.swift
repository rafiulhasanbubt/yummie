//
//  ListOrderViewController.swift
//  Yummie
//
//  Created by rafiul hasan on 5/11/21.
//

import UIKit

class ListOrderViewController: UIViewController {
    
    @IBOutlet weak var orderTableView: UITableView!
    
    var orders: [Order] = [
        .init(name: "Rafi khan", dish:.init(name: "Rafiul", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", image: #imageLiteral(resourceName: "iTunesArtwork"), calories: 130.722)),
        .init(name: "Rafi khan", dish:.init(name: "Rafiul", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", image: #imageLiteral(resourceName: "iTunesArtwork"), calories: 130.722)),
        .init(name: "Rafi khan", dish:.init(name: "Rafiul", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", image: #imageLiteral(resourceName: "iTunesArtwork"), calories: 130.722)),
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        orderTableView.dataSource = self
        orderTableView.delegate = self
        // Do any additional setup after loading the view.
        
        title = "Orders"
        registerCells()
    }

    private func registerCells() {
        orderTableView.register(UINib(nibName: DishListTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: DishListTableViewCell.identifier)
    }
    
}


extension ListOrderViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DishListTableViewCell.identifier, for: indexPath) as! DishListTableViewCell
        cell.setup(order: orders[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = DishDetailViewController.instantiate()
        controller.dish = orders[indexPath.row].dish
        navigationController?.pushViewController(controller, animated: true)
    }
    
}
