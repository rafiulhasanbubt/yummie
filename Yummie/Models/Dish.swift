//
//  Dish.swift
//  Yummie
//
//  Created by rafiul hasan on 5/11/21.
//

import Foundation
import UIKit

struct Dish {
    let name, description: String
    let image: UIImage
    let calories: Double
    
    var formattedCalories: String {
        return String(format: "%.2f Calories", calories )
    }
}
