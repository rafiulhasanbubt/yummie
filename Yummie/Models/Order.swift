//
//  Order.swift
//  Yummie
//
//  Created by rafiul hasan on 5/11/21.
//

import Foundation

struct Order {
    let name: String?
    let dish: Dish?
}
