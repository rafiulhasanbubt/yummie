//
//  DishCategory.swift
//  Yummie
//
//  Created by rafiul hasan on 5/11/21.
//

import Foundation
import UIKit

struct DishCategory {
    var title: String
    var image: UIImage
}
