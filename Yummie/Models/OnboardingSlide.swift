//
//  OnboardingSlide.swift
//  Yummie
//
//  Created by rafiul hasan on 5/10/21.
//

import Foundation
import UIKit

struct OnboardingSlide {
    let title: String
    let description: String
    let image: UIImage
}
