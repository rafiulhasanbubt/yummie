//
//  StringExtension.swift
//  Yummie
//
//  Created by rafiul hasan on 5/11/21.
//

import Foundation

extension String {
    var asURL: URL? {
        return URL(string: self)
    }
}
