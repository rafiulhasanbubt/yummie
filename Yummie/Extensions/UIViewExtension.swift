//
//  UIViewExtension.swift
//  Yummie
//
//  Created by rafiul hasan on 5/10/21.
//

import UIKit

extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get { return self.cornerRadius }
        set {
            self.layer.cornerRadius = newValue
        }
    }
}
