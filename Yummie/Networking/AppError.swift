//
//  AppError.swift
//  Yummie
//
//  Created by rafiul hasan on 5/30/21.
//

import Foundation

enum AppError: LocalizedError {
    case errorDecoding
    case unknownError
    case inavlidUrl
    case serverError(String)
    
    var errorDescription: String? {
        switch self {
        case .errorDecoding:
            return "Response could not be decode"
        case .unknownError:
            return "I have no idea what go on."
        case .inavlidUrl:
            return "Hey!! Give me a valid URL."
        case .serverError(let error):
            return error
        }
    }
}
