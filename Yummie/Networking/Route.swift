//
//  Route.swift
//  Yummie
//
//  Created by rafiul hasan on 5/30/21.
//

import Foundation

enum Route {
    static let baseUrl = "https://yummie.glitch.me"
    case temp
    var description: String {
        switch self {
        case .temp: return "/dishes/cat1"
        }
    }
}
