//
//  DishPortraitCollectionViewCell.swift
//  Yummie
//
//  Created by rafiul hasan on 5/11/21.
//

import UIKit

class DishPortraitCollectionViewCell: UICollectionViewCell {

    static let dishIdentifier = String(describing: DishPortraitCollectionViewCell.self)
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var dishImageView: UIImageView!
    @IBOutlet weak var caloriesLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func dishSetup(dish: Dish) {
        titleLbl.text = dish.name
        dishImageView.image = dish.image
        caloriesLbl.text = dish.formattedCalories
        descriptionLbl.text = dish.description
    }

}
