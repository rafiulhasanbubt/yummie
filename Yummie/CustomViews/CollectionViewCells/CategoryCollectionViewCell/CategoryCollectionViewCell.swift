//
//  CategoryCollectionViewCell.swift
//  Yummie
//
//  Created by rafiul hasan on 5/11/21.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {

    static let identifier = String(describing: CategoryCollectionViewCell.self)
    
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var categoryTitleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setup( category: DishCategory){
        categoryTitleLbl.text = category.title
        categoryImageView.image = category.image
    }
}
