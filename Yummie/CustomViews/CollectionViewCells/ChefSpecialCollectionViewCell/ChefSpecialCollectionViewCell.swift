//
//  ChefSpecialCollectionViewCell.swift
//  Yummie
//
//  Created by rafiul hasan on 5/11/21.
//

import UIKit

class ChefSpecialCollectionViewCell: UICollectionViewCell {
    
    static let identifier = String(describing: ChefSpecialCollectionViewCell.self)

    @IBOutlet weak var chefImageView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var caloriesLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setup(chef: Dish) {
        chefImageView.image = chef.image
        titleLbl.text = chef.name
        caloriesLbl.text = chef.formattedCalories
        descriptionLbl.text = chef.description
    }
}
