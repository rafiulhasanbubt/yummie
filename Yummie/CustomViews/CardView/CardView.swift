//
//  CardView.swift
//  Yummie
//
//  Created by rafiul hasan on 5/11/21.
//

import Foundation
import UIKit

class CardView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialSetup()
    }
    
    func initialSetup() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.1
        layer.shadowOffset = .zero
        layer.cornerRadius = 10
        layer.shadowRadius = 10
        cornerRadius = 10
    }
}
