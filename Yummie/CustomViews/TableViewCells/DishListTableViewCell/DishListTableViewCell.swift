//
//  DishListTableViewCell.swift
//  Yummie
//
//  Created by rafiul hasan on 5/11/21.
//

import UIKit

class DishListTableViewCell: UITableViewCell {

    static let identifier = String(describing: DishListTableViewCell.self)
    
    @IBOutlet weak var dishImageView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setup(dish: Dish) {
        dishImageView.image = dish.image
        titleLbl.text = dish.name
        descriptionLbl.text = dish.description
    }
    
    func setup(order: Order) {
        dishImageView.image = order.dish?.image
        titleLbl.text = order.dish?.name
        descriptionLbl.text = order.dish?.description
    }
    
}
